﻿namespace Cys_Common.Settings
{
    public class GlobalInfo
    {
        public static DownloadSetting DownloadSetting { get; set; } = new DownloadSetting();
        public static SearchEngineSetting SearchEngineSetting { get; set; } = new SearchEngineSetting();
        public static FavoritesSetting FavoritesSetting { get; set; } = new FavoritesSetting();
    }
}
